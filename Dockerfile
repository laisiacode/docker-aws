FROM docker:24.0.6-git

RUN apk add --no-cache python3 py3-pip
RUN pip3 install --no-cache-dir awscli
